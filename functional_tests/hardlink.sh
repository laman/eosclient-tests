#!/bin/bash
## EOS-1811: RFE: support for "hard links" in FUSE
set -e

# timeout for the whole test
sleep 10 && kill -SIGALRM "$$" &
sleeppid=$!

testdir=${1:-/eos/user/e/eostest/tests}/hardlink
mkdir -p "${testdir}"
linktmp=$(mktemp -d "${testdir}/${HOSTNAME}_hardlink_XXXX")

touch "${linktmp}/source"
/bin/ln "${linktmp}/source"  "${linktmp}/target"

rm -rf "${linktmp}"
echo "hardlink: all OK" >&2

kill "${sleeppid}" >&/dev/null ||:
exit 0
