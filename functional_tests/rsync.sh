#!/bin/bash
## try to rsync from local into EOS test area

set -e

# timeout for the whole test
sleep 60 && kill -SIGALRM "$$" &
sleeppid=$!

testdir=${1:-/eos/user/e/eostest/tests}/rsync
mkdir -p "${testdir}"
rsynctmp=$(mktemp -d "${testdir}/${HOSTNAME}_rsync_XXXX")
tmptmp="${rsynctmp}_tmp"
mkdir -p "${tmptmp}"
export TMP="${tmptmp}"
export TMPDIR="${tmptmp}"

rsynclocal=$(mktemp --tmpdir -d eos_rsync_XXXX)
cd "${rsynclocal}"
rpm2cpio http://linuxsoft.cern.ch/cern/centos/7/os/Source/SPackages/zlib-1.2.7-17.el7.src.rpm | cpio -id
tar xjf zlib-1.2.7.tar.bz2

rsync  --recursive --links --perms --times --group --owner zlib-1.2.7/ "${rsynctmp}"
diff -ur zlib-1.2.7/ "${rsynctmp}"

rm -rf "${rsynctmp}" "${tmptmp}"

echo "rsync: all OK" >&2

kill "${sleeppid}" >&/dev/null || :
exit 0
