#!/usr/bin/python

import os
import os.path
import random
import sys
import hashlib
import socket
import time
import math

import commands
import rpm
import argparse

parser = argparse.ArgumentParser(description='Check node status (EOS FUSE tests)')

host = socket.gethostname()
host = host.replace('.cern.ch','')
parser.add_argument("-v","--verbose", help="Verbose mode",action="store_false")


myargs = parser.parse_args()

verbose = myargs.verbose

host = socket.gethostname()
host = host.replace('.cern.ch','')

t0 = time.time()
print 'checkStatus starts at %s on %s' % (t0,host)

ts = rpm.TransactionSet()
mi = ts.dbMatch()
rpmlist = []
for h in mi:
    if 'xroot' in h['name'] or 'fuse' in h['name']:
        ll = "%s-%s-%s" % (h['name'], h['version'], h['release'])
        rpmlist.append(ll)

print 80*'-'
print '### Interesting RPMs:'
rpmlist.sort()
for r in rpmlist:
    print r
print 80*'-'
unameout = commands.getoutput('uname -a')
print unameout
print 80*'-'
statout = commands.getoutput('stat /eos/user/l/lamanna/')
print statout
print 80*'-'
dfout = commands.getoutput('df')
print dfout
print 80*'-'

t1 = time.time()
print 'checkStatus ended at %s (elapsed %.1fs)' %(t1,t1-t0)

    
