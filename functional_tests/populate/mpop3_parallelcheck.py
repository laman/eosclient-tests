#!/usr/bin/python
import os
import time
import socket
import random
import uuid

import threading

import sys

import hashlib

import glob
import re
from os import walk
import time

import random

import argparse

eosserverdata = {}

#
# https://stackoverflow.com/questions/26894024/subprocess-check-output-module-object-has-out-attribute-check-output
#
def check_output(command_list):
    proc = subprocess.Popen(command_list, stdout=subprocess.PIPE)
    proc.wait()
    return proc.stdout.read()

def checkdir(rdir,verbose):

    wdir = '/eos/user/l/laman/EOStests'
    if os.path.exists('%s/touch.mpop3_check.%s' % (wdir,host)) or os.path.exists('%s/touch.mpop3_check' % wdir ):

        if verbose: print 'End of execution (touch file found)'
        return -1,-1,-1

    if verbose: print 'Analysing %s' % rdir
    aaa = emd.search(rdir)
    if verbose: print aaa
    try:
        fsize = int(aaa.group(1))
        nd    = int(aaa.group(2))
        nf    = int(aaa.group(3))
    except AttributeError:
        print 'Invalid directory: %s' % rdir
        return -1

    bn = os.path.basename(rdir)
    if verbose: print '\tdir %s: ndir=%d and nfiles=%d' % (rdir,nd,nf)

    return nd,nf,fsize


def preparetree(rdir):
    f = []
    kd = 1
    for (dirpath, dirnames, filenames) in walk(rdir):
        for fn in filenames:
            f.append(dirpath+os.sep+fn)
        kd += len(dirnames)
    return f,len(f),kd

def xroot_preparetree(rdir):
    print 'xroot_preparetree'
    cmd = ["eos","root://"+server,"find",rdir]
    #print cmd
    proc = Popen(cmd, stdout=PIPE)
    out = proc.communicate()[0].split()
    f = []
    kd = 0
    for i in out:
        if i[0:5] == 'path=': 
            kd +=1
        else: 
            f.append(i)
    return f,len(f),kd

def analysetree(f,fsize):
    kf = 0
    errormsg = set()
    ta = time.time()
    rc = 0
    kprint = 1
    for i in f:
        if verbose: print 'analysetree: Analyse file %s of size %d' %(i,fsize)
        try:
            filename = i
            if not useFuse:
                server = eosserverdata['server']
                path = eosserverdata['path']
                filename = '/tmp/%s' % os.path.basename(i)
                cmd = "xrdcp -sf root://%s.cern.ch/%s %s" % (server,i,filename) # silent * allow overwrite
                #print cmd
                rc = subprocess.call(cmd,shell=True,stdout=open(os.devnull, 'wb'))
            #print filename
            fh = os.open(filename,os.O_RDONLY)#|os.O_DIRECT)
            x=os.read(fh,fsize+1)
        except OSError as e:
            print 'Error in opening/reading file %s' % i
            print "OS error({0}): {1}".format(e.errno, e.strerror)
            sys.exit(1)
        m = hashlib.md5()
        m.update(x)
        md5 = m.hexdigest()
        n1=i.rfind('/')
        n2=i.rfind('.')
        emd5 = i[n1+1:n2]
        if fsize!=len(x):
            msg = 'Error in reading %s (wrong size: %s instead of %s)' % (i,len(x),fsize)
            print msg
            errormsg.add(msg)
            rc += 100
        if emd5!=md5:
            msg = 'Error in reading %s (wrong checksum: %s)' % (i,md5)
            print msg
            errormsg.add(msg)
            rc += 10000

        msg = ""

        kf+=1

        if kf%kprint==0:
            tb = time.time()
            dt = tb - ta
            ta = tb
            hz = float(kprint)/dt*0.9  # *0.9 because #files = kprint -  kprint/10
            kprint *=10
            if verbose: print 'Analysed %d files so far in %d s (%.2f Hz)' % (kf,dt,hz)
        sys.stdout.flush()

        os.close(fh)
        if not useFuse: os.remove(filename)
    return rc,kf,errormsg

def analysedir(rdir,nd,nf,fsize):

    #errorfile = set()
    errormsg = set()

    rc = 0

    t0 = time.time()

    if useFuse:
        f,kf,kd = preparetree(rdir)
    else:
        f,kf,kd = xroot_preparetree(rdir)

    msg = ""

    if kd==nd:
        if verbose: print 'All dirs found'
    else:
        msg = 'Expected %d directories in %s: %d found' % (nd,rdir,kd)
        print msg
        errormsg.add(msg)
        rc += 1
    if len(f)==nf:
        if verbose: print 'All files found'
    else:
        msg = 'Expected %d files in %s: %d found' % (nf,rdir,len(f))
        errormsg.add(msg)
        print msg
        rc += 10

    msg = ""

    print 'Start checksumming %d files at %s' % (len(f),time.ctime())

    kf = 0

    ta = time.time()

    rc,kf,berrors = analysetree(f,fsize)

    for e in berrors:
        errormsg.add(e)
    
    t1 = time.time()
    hz=-1
    if t1>t0:
        hz=kf/(t1-t0)
    print 'Elapsed time %d s (visiting %d dirs and checksumming %d files at %.2f Hz)' % (t1-t0,kd,len(f),hz)
    if len(errormsg)>0:
        print '### Error summary'
        for msg in errormsg:
            print '###',msg
        print '###'


def worker(num,nt,ld,verbose,parallelmount):
    """thread worker function"""

    assert num<nt and num>=0, 'Bail out worker num=%d nt=%d' % (num,nt)

    for i in xrange(num,len(ld),nt):
        dn = ld[i]  # Pick up one file each nt for mount num
        print 'Worker %d/%d: check directory %s' % (num,nt,dn)

        newlead = '/var/tmp/uat'
        if parallelmount and dn.find(newlead)==0:
            newlead += '%02d' % (num)

            dn = newlead + dn[len(newlead):]

        elif parallelmount: 
            print 'Invalid path %s' % newlead
            sys.exit(1)

        nd,nf,fsize = checkdir(dn,verbose)

        if nd<0: 
            print 'Worker %d/%d exiting (touch file found)' % (num,nt)
            if verbose: print 'Skipping %s' %dn
            return
        print 'Worker %d/%d: analyse dir %s' % (num,nt,dn)
        analysedir(dn,nd,nf,fsize)
    print 'Worker %d/%d exiting (end of processing)' % (num,nt)
    return

parser = argparse.ArgumentParser(description='Check a tree created with mpop3.py')

parser.add_argument("-n","--nofthreads", type=int, default=1, help="Number of parallel reading threads")
parser.add_argument("directory", type=str, help="Input directory. \nIf it is given in the form root://eoshome.cern.ch:1095//eos/... eos commands and xrdcp are used to analyse the directory")
parser.add_argument("--shuffle", help="Shuffle input directories",action="store_true")
parser.add_argument("-v","--verbose", help="Verbose mode",action="store_true")
parser.add_argument("--parallelmount", help="Parallel mount (experimental)",action="store_true")

myargs = parser.parse_args()

verbose = myargs.verbose

dir0 = myargs.directory#.rstrip('/')

xrootstyle = re.compile("root://(\w+).cern.ch:*\d*/(/.+)")                                             

parallelmount = myargs.parallelmount

useFuse = True
aaa = xrootstyle.search(dir0)
if (aaa):
    useFuse =  False
    import subprocess
    from XRootD import client
    from XRootD.client.flags import QueryCode, OpenFlags
    
    eosserverdata['server'] = aaa.group(1)
    eosserverdata['path']   = aaa.group(2)

    dir0 = eosserverdata['path']

    if verbose:
        print 'xroot path'
        print 'server',eosserverdata['server']
        #print 'port',aaa.group(2)
        print 'path',eosserverdata['server']
        print 'use fuse?',useFuse

nt = myargs.nofthreads

if parallelmount:
    for i in range(nt):

        if verbose: 'Checking up mount for thread %d' % i

        eosxconf = '/eos/user/l/laman/.eos/fuse.uat%02d.conf' % i

        if verbose: print 'File %s must exist' % eosxconf

        assert os.path.isfile(eosxconf),'Cannot find the config file: %s' % eosxconf
            
        eosxmount = '/var/tmp/uat%02d' % i
        eosxcmd   = 'eosxd -ofsname=uat%02d %s' % (i,eosxmount)

        print 'Checking/creating private mount point:',eosxmount
        try:
            os.mkdir(eosxmount)
        except OSError:
            pass
        
        print 'Private mount should be enable with command:',eosxcmd
        #os.system(eosxcmd)
        
emd = re.compile("_fsize:(\d+)_dirs:(\d+)_files:(\d+)")

if verbose: print 'Scanning %s dir' % dir0
if dir0[-1:]=='/': dir0 = dir0[:-1] #Not protected for multiple trailing slashes

if useFuse:
    ld = glob.glob(dir0+'*')
else:
    from subprocess import PIPE,Popen
    server = eosserverdata['server']      
    cmd = ["eos","root://"+server,"ls","-d", dir0+'']
    #print cmd
    proc = Popen(cmd, stdout=PIPE)
    out = proc.communicate()[0].split()
    ld = []
    for i in out:
        ld.append(os.path.dirname(dir0)+'/'+i)

if myargs.shuffle:
    if verbose: print 'Shuffle directories...'
    random.shuffle(ld)

if verbose:
    print 'Directories to be scanned:'
    for rdir in ld:
        print rdir
    print '---'

host = socket.gethostname()
host = host.replace('.cern.ch','')

print '# mpop starts at %s on %s' % (time.ctime(),host)
#print 'EOS_MGM_URL:',os.environ['EOS_MGM_URL']

threads = []

for i in range(nt):
    if verbose: print 'Assigning dirs to thread %d/%d' % (i,nt)
    t = threading.Thread(target=worker, args=(i,nt,ld,verbose,parallelmount,))
    threads.append(t)
    t.start()

sys.exit(0)

