#!/bin/bash

TS=`date +'%s'`
DS=`date +'%b%d'`
MPOP3TOP='/eos/pps/users/laman/run_test2/FUSEX/'
###MPOP3TOP='/eos/scratch/user/l/laman/run_test/FUSEX/'
MPOP3DIR=$MPOP3TOP$DS

mkdir -p $MPOP3TOP

echo
echo

echo '# Creating tree as '$MPOP3DIR'...'
./mpop3.py -l4 -d3 -f20 -s4048 $MPOP3DIR | tee /tmp/run_mpop3.log.$TS
RC1=$?

MPOP3TEST=`grep 'Writing in' /tmp/run_mpop3.log.$TS | cut -c 12-`

#MPOP3TEST=`find $MPOP3 -maxdepth 1 -type d -mmin -1440 -mmin +30 -ls | grep $DS | sort -R | tail -1 | awk '{print $NF}'`
MPOP3TEST=`find $MPOP3TOP -maxdepth 1 -type d -ls | grep $DS | sort -R | tail -1 | awk '{print $NF}'`

echo '# Checking '$MPOP3TEST
./mpop3_check.py $MPOP3TEST | tee /tmp/run_mpop3check.log.$TS
RC2=$?

echo
TT=`date -d @$TS`
echo $TT,' UnixTime = '$TS,' RC1 = '$RC1,' RC2 = '$RC2,' MPOP3 test = '$MPOP3TEST

if [ "$RC1" == '0' ] && [ "$RC2" == '0' ]; then
    rm /tmp/run_mpop3.log.$TS
    rm /tmp/run_mpop3check.log.$TS
fi
