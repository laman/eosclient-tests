#!/usr/bin/python

import os
import os.path
import random
import sys
import hashlib
import socket
import time
import math
import re

eosserverdata = {}

def createfile(rdir,nbytes,number_of_files,blocksize=1024):

    nblocks = nbytes/blocksize
    nr = nbytes%blocksize

    ofs = []

    assert nblocks*blocksize+nr==nbytes,'Chunking error!'

    for i in range(number_of_files):
        block_data = str(os.urandom(blocksize))     # Repeated nblocks times
        block_data_r = str(os.urandom(nr))   # Only once
        m = hashlib.md5()
        for kb in range(nblocks):
            m.update(block_data)
        m.update(block_data_r)
        md5 = m.hexdigest()
        
        of = rdir+os.sep+md5+'.dat'
        fh = file(of,'wb')
        for i in range(nblocks):
            try:
                fh.write(block_data)
            except IOError:
                print 'Error in writing block %d in file %s' % (i,of)
                time.sleep(0.1)
                fh.write(block_data)
                print 'Retry successful...'

        try:
            fh.write(block_data_r)            
        except IOError:
            print 'Error in writing the last block in file %s' % (of)
            time.sleep(0.1)
            fh.write(block_data_r)
            print 'Retry successful...'

        try:
            fh.close()
        except IOError:
            print 'Error in writing closing %s' % of
            time.sleep(1)
            fh.close()
            print 'Retry successful...'

        ofs.append(of)
    return ofs

def dive(rdir,nl,nd,nf,size,blocksize):

    if useFuse:
        if not os.path.exists(rdir):
            if verbose: print 'Dive: create root dir %s' % rdir
            os.mkdir(rdir)
    else:
        server = eosserverdata['server']
        path = eosserverdata['path']
        rc = subprocess.call(["eos","root://"+server,"stat", rdir],stdout=open(os.devnull, 'wb'))
        if rc!=0:
            print rdir
            rc = subprocess.call(["eos","root://"+server,"mkdir", rdir],stdout=open(os.devnull, 'wb'))

    if useFuse:
        createfile(rdir,size,nf,blocksize)
    else:
        flist = createfile('/tmp',size,nf,blocksize)
        for fn in flist:
            server = eosserverdata['server']
            path = eosserverdata['path']
            cmd = "xrdcp -sf %s root://%s.cern.ch/%s/." % (fn,server,rdir) # silent * allow_override
            #print cmd
            rc = subprocess.call(cmd,shell=True,stdout=open(os.devnull, 'wb'))
 #           rc = subprocess.call("xrdcp","-f",fn,"root://%s.cern.ch/%s/." % (server,rdir))
            os.remove(fn)
        
    if nl<=0: return

    for i in range(nd):
        newdir = '%s%s%d' % (rdir,os.sep,i)
        try:
            if useFuse:
                os.mkdir(newdir)
            else:
                rc = subprocess.call(["eos","root://"+server,"mkdir", newdir])
            dive(newdir,nl-1,nd,nf,size,blocksize)
        except OSError as e:
            print 'Exception in trying to create %s' % newdir
            print 'Python exception: %s' % str(e)
            print 'Check again'
            print 'Path %s exist? %s'    % (newdir,os.path.exists(newdir))
            print 'Path %s is a dir? %s' % (newdir,os.path.isdir(newdir))
            print 'Bailing out at %s' % time.ctime()
            print '--> tail /var/log/eos/fusex/fuse.pps.log <--'
            os .system('tail /var/log/eos/fusex/fuse.pps.log')
            print 80*'-'
            print 80*'-'
            print 80*'-'
            sys.exit(1000)


def bigdive(dir0,nlevel,ndirsperlevel,nfilesperdir,size,blocksize):

    if verbose: print 'Starting tree creation'
    tfile = 'touch.mpop3'
    
    while(True):

        if os.path.exists('%s.%s' % (tfile,host)) or os.path.exists(tfile):
            if verbose: print 'touch file %s.* found (stop execution)' % tfile
            return

        rr = int(random.uniform(0.,1.)*10000000)

        rdir = '%s_%s_%06d_%s_%s_%s_fsize:%s_dirs:%s_files:%s' % (dir0,host,rr,nlevel,ndirsperlevel,nfilesperdir,size,totdir,totfile)

        print 'Writing in %s' % rdir

        t0=time.time()
        dive(rdir,nlevel,ndirsperlevel,nfilesperdir,size,blocksize)
        et = time.time()-t0
        cr = -1.
        wr = -1.
        wmb = totfile*size/1E+6 # MB written
        if et>0: 
            cr = (totdir+totfile)/et
            wr = wmb/et
        ts = time.strftime('%H:%M %d/%m/%y')
        print '%s: elapsed time %d s (%.1f Hz in creating %d directories and %d files; %.1f MB/s)' % (ts,et,cr,totdir,totfile,wr)

        if oneshot: return

import argparse

parser = argparse.ArgumentParser(description='Create a tree with checksum files')

host = socket.gethostname()
host = host.replace('.cern.ch','')

parser.add_argument("directory", type=str, help="Input directory. \nIf it is given in the form root://eoshome.cern.ch:1095//eos/... eos commands and xrdcp are used to populate the directory")

parser.add_argument("-l", "--levels", type=int, default=6, help="Subdirectory levels")
parser.add_argument("-d", "--ndirsperlevel", type=int, default=2, help="Number of subdirectories to be create at each level")
parser.add_argument("-f", "--nfilesperdir", type=int, default=3, help="Number of checksum files to be created at each level")
parser.add_argument("-s", "--size", type=str, default='1024', help="Checksum size in bytes (MB extension accepted 1MB=1,000,000")
parser.add_argument(      "--blocksize", type=str, default='1024', help="Write blocksize (MB extension accepted 1MB=1,000,000")

parser.add_argument("-v","--verbose", help="Verbose mode",action="store_true")

parser.add_argument("--oneshot", help="Single execution (default - compatibility only)",action="store_false")
parser.add_argument("--loop", help="Repeat command in an infinite loop",action="store_true")

myargs = parser.parse_args()

nlevel = myargs.levels
ndirsperlevel = myargs.ndirsperlevel
nfilesperdir  = myargs.nfilesperdir
size = myargs.size
bs = myargs.blocksize

verbose = myargs.verbose

if bs.find('MB')>-1:
    bs = int( bs.replace('MB','').strip()) *1E+6
if size.find('MB')>-1:
    size = int( size.replace('MB','').strip()) *1E+6
size = int(size)
bs   = int(bs)

oneshot = True
loop = myargs.loop
if loop: oneshot = False

dir0 = myargs.directory

xrootstyle = re.compile("root://(\w+).cern.ch:*\d*/(/.+)")

useFuse = True
aaa = xrootstyle.search(dir0)
if (aaa):
    useFuse =  False
    import subprocess
    from XRootD import client
    from XRootD.client.flags import QueryCode, OpenFlags
    
    eosserverdata['server'] = aaa.group(1)
    eosserverdata['path']   = aaa.group(2)

    dir0 = eosserverdata['path']

    if verbose:
        print 'xroot path'
        print 'server',eosserverdata['server']
        #print 'port',aaa.group(2)
        print 'path',eosserverdata['server']
        print 'use fuse?',useFuse

if ndirsperlevel>1:
    totdir = int(( math.pow(ndirsperlevel,nlevel+1)-1)/(ndirsperlevel-1))
elif ndirsperlevel==1:
    totdir = nlevel+1
else:
    sys.exit('Invalid ndirsperlevel')

totfile = totdir*nfilesperdir

host = socket.gethostname()
host = host.replace('.cern.ch','')

print 'Execution starts at',time.ctime(),'on',host
print 'No. of dirs to be created:', totdir
print 'No. of files to be created:', totfile
if verbose:
    print 'Verbose mode ON'
    print 'nlevel: %d' % nlevel
    print 'ndirperlevel: %d' % ndirsperlevel
    print 'nfilesperdir: %d' % nfilesperdir
    print 'size: %d'      % size
    print 'blocksize: %d' % bs
    
bigdive(dir0,nlevel,ndirsperlevel,nfilesperdir,size,bs)
sys.exit(0)
    
