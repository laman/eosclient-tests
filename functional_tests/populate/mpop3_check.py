#!/usr/bin/python

import os
import sys
import hashlib

import glob
import re
from os import walk
import time

import argparse

parser = argparse.ArgumentParser(description='Check a tree created with mpop3.py')


parser.add_argument("directory", type=str, help="Input directory")

parser.add_argument("--verbose", help="Verbose mode",action="store_true")

myargs = parser.parse_args()

dir0 = myargs.directory.rstrip('/')

verbose = myargs.verbose

emd = re.compile("_fsize:(\d+)_dirs:(\d+)_files:(\d+)")

ld = glob.glob(dir0+'*')

if verbose:
    print 'Directories to be scanned:'
    for rdir in ld:
        print rdir
    print '---'

rc = 0

for rdir in ld:
    if verbose: print 'Analysing %s' % rdir
    aaa = emd.search(rdir)
    fsize = int(aaa.group(1))
    nd = int(aaa.group(2))
    nf = int(aaa.group(3))

    print 'Analyse %s at %s (expecting ndir=%d,nfiles=%d)' % (rdir,time.ctime(),nd,nf)

    f = []
    kd = 1
    t0 = time.time()
    for (dirpath, dirnames, filenames) in walk(rdir):
        for fn in filenames:
            f.append(dirpath+os.sep+fn)
        kd += len(dirnames)
    if kd==nd:
        if verbose: print 'All dirs found'
    else:
        print '\tExpected %d directories in %s: %d found' % (nd,rdir,kd)
        rc += 1
    if len(f)==nf:
        if verbose: print 'All files found'
    else:
        print '\tExpected %d files in %s: %d found' % (nf,rdir,len(f))
        rc += 10

    print 'Start checksumming %d files at %s' % (len(f),time.ctime())

    kf = 0

    ta = time.time()

    kprint = 1
    
    for i in f:
        #if verbose: print 'Analyse file %s of size %d' %(i,fsize)
        fh = os.open(i,os.O_RDONLY)#|os.O_DIRECT)
        x=os.read(fh,fsize+1)
        m = hashlib.md5()
        m.update(x)
        md5 = m.hexdigest()
        n1=i.rfind('/')
        n2=i.rfind('.')
        emd5 = i[n1+1:n2]
        if fsize!=len(x):
            print 'Error in reading %s (wrong size: %s instead of %s)' % (i,len(x),fsize)
            rc += 100
        if emd5!=md5:
            print 'Error in reading %s (wrong checksum: %s)' % (i,md5)
            rc += 10000

        kf+=1

        if kf%kprint==0:
            tb = time.time()
            dt = tb - ta
            ta = tb
            hz = float(kprint)/dt*0.9  # *0.9 because #files = kprint -  kprint/10
            kprint *=10
            if verbose: print 'Analysed %d files so far in %d s (%.2f Hz)' % (kf,dt,hz)
        sys.stdout.flush()

        os.close(fh)
    t1 = time.time()
    hz=-1
    if t1>t0:
        hz=kf/(t1-t0)
    print 'Elapsed time %d s (visiting %d dirs and checksumming %d files at %.2f Hz)' % (t1-t0,kd,len(f),hz)
    sys.exit(rc)



