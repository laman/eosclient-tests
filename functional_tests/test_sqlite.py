#!/usr/bin/python
#
## based on INC1084899, R De Maria
## /afs/cern.ch/user/r/rdemaria/public/test_sqlite/test_sqlite.py
## with EOS path hardcoded

import sqlite3,os,errno,sys,signal

dir='/eos/user/e/eostest/tests/sqlite3'
if len(sys.argv) > 1:
  dir=sys.argv[1]+'/sqlite3'

# 60sec timeout
signal.alarm(60)

try:
  os.makedirs(dir)
except OSError as exc:
  if exc.errno == errno.EEXIST and os.path.isdir(dir):
    pass
  else:
    raise

if os.path.exists(dir+'test.db'):
  os.unlink(dir+'test.db')

db=sqlite3.connect(dir+'test.db',isolation_level=None)

db.execute('create table t(a,b)')

for i in range(10):
  print(i)
  db.executemany('insert into t values (?,?)',zip([i]*10,range(10)))


print db.execute('select * from t').fetchall()

db.close()






