#!/bin/bash
## download and recompile zlib (this runs the internal self-tests at the end)
## supposed to reproduce a small project compilation on EOS
set -e

# timeout for the whole test
sleep 120 && kill -SIGALRM "$$" &
sleeppid=$!

testdir=${1:-/eos/user/e/eostest/tests}/zlib
mkdir -p "${testdir}"
rpmtmp=$(mktemp -d "${testdir}/${HOSTNAME}_zlib_XXXX")
tmptmp="${rpmtmp}/tmp"
mkdir -p ${tmptmp}
export TMP="${tmptmp}"
export TMPDIR="${tmptmp}"

rpmbuild --rebuild --define "_topdir ${rpmtmp}" http://linuxsoft.cern.ch/cern/centos/7/os/Source/SPackages/zlib-1.2.7-17.el7.src.rpm

rm -rf "${rpmtmp}"
echo "zlib: all OK" >&2

kill "${sleeppid}" >&/dev/null ||:
exit 0
