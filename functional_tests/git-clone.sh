#!/bin/bash
## simple 'git clone' (which may expose race conditions)
set -e
# timeout for the whole test
sleep 60 && kill -SIGALRM "$$" &
sleeppid=$!

testdir=${1:-/eos/user/e/eostest/tests}/git-clone
mkdir -p "${testdir}"|| exit 1
gittmp=$(mktemp -d "${testdir}/${HOSTNAME}_git-clone_XXXX")
tmptmp="${gittmp}_tmp"
mkdir -p "${tmptmp}"
export TMP="${tmptmp}"
export TMPDIR="${tmptmp}"

git clone "https://:@gitlab.cern.ch:8443/dss/eosclient-tests.git" "${gittmp}"
pushd "${gittmp}"
git pull
popd

rm -rf "${gittmp}" "${tmptmp}"
echo "git-clone: all OK" >&2

kill "${sleeppid}" >&/dev/null ||:
exit 0
