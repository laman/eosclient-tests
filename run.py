#!/usr/bin/env python

import os
import commands
import time
import tempfile
import shutil
import socket
import argparse
import glob
import sys

test_def = {
  'all' : '*_tests/*',
  'all_excluded' : '',

  'microbench' : "microbench_tests/*",
  'microbench_excluded' : '',

  'ci' : "microbench_tests/*",
  'ci_excluded' : "microbench_tests/*_fortran_*"
  }

BINDIR=os.path.dirname(__file__)

parser = argparse.ArgumentParser()
parser.add_argument('--workdir', action='store', dest='workdir', default=BINDIR, help='eos fuse mount directory to run the tests (one or more directories separated by spaces)')
parser.add_argument('--monitoring', action='store_true', default=False, dest='monitoring', help='enable monitoring')
parser.add_argument('--silent', action='store_true', default=False, dest='silent', help='print errors only')
parser.add_argument('--instance-name',action='store',default="eos", dest='instance_name',help='instance name for reporting')
parser.add_argument('--disable-exitcode', action='store_false', default=True, dest='report_exitcode', help='disable reporting non-zero exit codes in case of failed tests')
parser.add_argument('test',action='store',default=None, help="test to run: %s" % ",".join(sorted([x for x in test_def.keys() if "_excluded" not in x])))

args = parser.parse_args()

def expand_pattern(name):
  files=[]
  for pattern in test_def[name].split(" "):
    for t in glob.glob(os.path.join(BINDIR,pattern)):
      files.append(os.path.abspath(t))
  return files

tests = expand_pattern(args.test)
excluded_tests = expand_pattern(args.test+"_excluded")

tests = sorted(list(set(tests)-set(excluded_tests)))

WORKDIRS=[os.path.abspath(tempfile.mkdtemp(dir=d)) for d in args.workdir.split()]

CARBON_HOST = 'filer-carbon.cern.ch'
CARBON_PORT = 2003

def send_message(message, timestamp = None):
  if timestamp is None:
    timestamp = time.time()

#  print '%s %d' % (message, timestamp)
  try:
    conn = socket.create_connection((CARBON_HOST, CARBON_PORT))
    conn.send('%s %d\n' % (message, timestamp))
    conn.close()
  except socket.error:
    # ignore any connection errors to avoid spamming
    pass

for d in WORKDIRS:
  if not os.path.isdir(d):
    raise Exception('WORKDIR: %s does not exist' % d)

success = True

for bin in tests:
  test = os.path.basename(bin)
  cmd = 'timeout 300 %s %s' % (bin, " ".join(WORKDIRS))

  if not args.silent:
    print 'running',test

  start = time.time()
  status, output = commands.getstatusoutput(cmd)
  end = time.time()
  duration = end - start

  success = success and status==0

  if not args.silent:
    message = 'eos.clients.fuse.%s.%s_ms time:%.3f\n status: %d\n command: %s\n output: %s\n' % (args.instance_name, test, duration * 1000, status, cmd, output)
    print message

  if args.monitoring:
    testname = test.split('_', 1)[1]
    message = 'eos.clients.fuse.%s.microtests.%s_ms %.3f' % (args.instance_name, testname, duration * 1000)
    send_message(message, int(start))
  

# don't rm the testdir, do in a daily cron
#shutil.rmtree(WORKDIR)

if success or not args.report_exitcode:
  sys.exit(0)
else:
  sys.exit(1)
